// https://jestjs.io/docs/getting-started
// https://github.com/kulshekhar/ts-jest

/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
};

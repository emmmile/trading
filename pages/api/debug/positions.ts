import type { NextApiRequest, NextApiResponse } from "next";
import Api from "../../../library/api/api";
import { Currency } from "../../../library/types";

export default async function Positions(req: NextApiRequest, res: NextApiResponse) {
  // http://localhost:3000/api/positions/
  const currency: Currency = Currency[process.env.CURRENCY!];
  const api = Api.create();
  const positions = await api.allPositions(currency);

  res.json(positions);
  res.statusCode = 200;
}

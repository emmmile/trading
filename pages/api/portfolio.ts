import type { NextApiRequest, NextApiResponse } from "next";

import retry from "async-retry";
import { sumBy } from "../../library/arithmetic";
import { AccountType, Currency } from "../../library/types";
import Api from "../../library/api/api";
import PositionsApi, { Position } from "../../library/api/positions";
import AccountsApi from "../../library/api/accounts";
import { log } from "../../library/logger";

export type TargetPortfolio = Array<any>; // TODO
export type PortfolioDetailsByAccount = {
  hashedAccountId: string;
  accountType: AccountType;
  current: number;
  currentPercent: number;
  matchingPositions: Position[];
};
export type PortfolioItem = {
  category: string;
  name: string;
  targetPercent: number;
  target: number;
  currentPercent: number;
  current: number;
  // matchingNames: string[];
  byAccount: PortfolioDetailsByAccount[];
};

const targetPortfolio = require("../../data/target-portfolio.json");

const retryOptions = {
  retries: 5,
  factor: 2,
  // onRetry: (error) => log.error(error),
};

export async function getAllDataWithRetries(currency: Currency): Promise<Position[]> {
  const api = Api.create();

  const retryCall = (message: string, logic: () => Promise<any>) => {
    return retry(async (_bail) => {
      log.debug(message);
      try {
        return await logic();
      } catch (e) {
        log.error(e);
      }
    }, retryOptions);
  };

  const positions: Position[] = await retryCall("Trying to get positions data from the API.", () =>
    api.allPositions(currency)
  );

  const total = sumBy(
    positions,
    (p) => p?.value,
    (p) => p?.currency
  );
  log.debug(`Total positions value is ${total.toFixed(2)} ${currency}.`);
  log.info("Successfully gotten all data from the API.");
  return positions;
}

export default async function Portfolio(req: NextApiRequest, res: NextApiResponse) {
  // http://localhost:3000/api/portfolio/
  // will get information for all my accounts and then filter out in the frontend
  const currency: Currency = Currency[process.env.CURRENCY!];

  const api = Api.create();
  // TODO handle errors
  const positions = await api.allPositions(currency);
  const portfolio = generatePortfolio(positions);

  res.json(portfolio);
  res.statusCode = 200;
}

export function generatePortfolio(positions: Position[]): PortfolioItem[] {
  const accountIds = AccountsApi.allAccountIds();
  const missing = missingPositions(positions, accountIds, targetPortfolio);

  const output = portfolioStatus(positions, missing, accountIds, targetPortfolio);
  checkPortfolio(output);
  return output;
}

function portfolioStatus(
  positions: Position[],
  missingPositions: Position[],
  accountIds: string[],
  targetPortfolio: TargetPortfolio
): PortfolioItem[] {
  const totalValue = sumBy(
    [...positions, ...missingPositions],
    (p: Position) => p.value,
    (p: Position) => p.currency
  );
  const output: PortfolioItem[] = [];

  targetPortfolio.map((pi) => {
    const matchingNames = pi.matchingNames;
    const matchingPositions = PositionsApi.filterPositions(
      positions,
      Api.hashedAccountIds(accountIds),
      matchingNames
    );
    const currentValue = sumBy(
      matchingPositions,
      (p: Position) => p.value,
      (p: Position) => p.currency
    );

    output.push({
      category: pi.category,
      name: pi.name,
      targetPercent: pi.targetPercent,
      target: (pi.targetPercent * totalValue) / 100.0,
      currentPercent: (currentValue / totalValue) * 100,
      current: currentValue,
      byAccount: portfolioInfoByAccount(matchingPositions, matchingNames),
    });
  });

  const missingPositionsNames = missingPositions.map((o) => o.name);
  const missingcurrent = sumBy(
    missingPositions,
    (p: Position) => p.value,
    (p: Position) => p.currency
  );

  output.push({
    category: "others",
    name: "unknown",
    targetPercent: 0,
    target: 0,
    currentPercent: (missingcurrent / totalValue) * 100,
    current: missingcurrent,
    byAccount: portfolioInfoByAccount(missingPositions, missingPositionsNames),
  });

  return output.filter((pi) => pi.current > 0 || pi.target > 0);
}

function portfolioInfoByAccount(
  positions: Position[],
  matchingNames: string[]
): PortfolioDetailsByAccount[] {
  return Api.hashedAccountIds(Api.allAccountIds())
    .map((a) => {
      const matchingPositions = PositionsApi.filterPositions(positions, [a], matchingNames);
      const current = sumBy(
        matchingPositions,
        (p: Position) => p.value,
        (p: Position) => p.currency
      );

      return {
        hashedAccountId: a,
        accountType: AccountType.SAVINGS, // TODO
        current: current,
        currentPercent: 0, // (current / a.currentValue) * 100,
        matchingPositions: matchingPositions,
      };
    })
    .filter((a) => a.matchingPositions.length != 0);
}

function missingPositions(
  positions: Position[],
  accountIds: string[],
  targetPortfolio: TargetPortfolio
): Position[] {
  const targetPortfolioPositions = targetPortfolio.flatMap((pi) => pi.matchingNames);

  const missing = positions
    .filter((p) => AccountsApi.hashedAccountIds(accountIds).includes(p.hashedAccountId))
    .filter((p) => !targetPortfolioPositions.includes(p.name));

  if (missing.length > 0) {
    log.warn(`There are ${missing.length} positions not matched by the portfolio:`);
    log.warn(`  ${JSON.stringify(missing)}`);
  }

  return missing;
}

function checkPortfolio(portfolioItems: PortfolioItem[]): void {
  const totalPercent = sumBy(portfolioItems, (pi: PortfolioItem) => pi.targetPercent, undefined);
  const totalValuePercent = sumBy(
    portfolioItems,
    (pi: PortfolioItem) => pi.currentPercent,
    undefined
  );

  if (totalPercent != 100) {
    console.warn(`Total portfolio target is ${totalPercent.toFixed(2)}% instead of 100%.`);
  }

  console.info(`Portfolio target summary:`);
  portfolioItems.forEach((pi) =>
    console.info(`  ${pi.name}: ${pi.targetPercent}% (${pi.currentPercent.toFixed(2)}%)`)
  );

  if (Math.abs(100 - totalValuePercent) > 0.1) {
    console.warn(`Total portfolio value is ${totalValuePercent}% instead of 100%.`);
  }
}

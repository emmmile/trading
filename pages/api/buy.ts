import type { NextApiRequest, NextApiResponse } from "next";

import Api from "../../library/api/api";
import { OrderType } from "degiro-api/dist/types";
import { DeGiroActions, DeGiroMarketOrderTypes, DeGiroTimeTypes } from "degiro-api/dist/enums";

// TODO move to a configuration file
const lastOrder = 365 / 24; // days since last order, ~15 days
const productsToBuy = {
  // "15695920": 100, // iShares MSCI World SRI UCITS ETF EUR (Acc)
};

export default async function Buy(req: NextApiRequest, res: NextApiResponse) {
  // http://localhost:3000/api/buy/
  const api = Api.create();
  const degiroApi = await api.degiroApi();
  const date = new Date();

  try {
    const { data } = await degiroApi.getHistoricalOrders({
      toDate: `${String(date.getDate()).padStart(2, "0")}/${String(date.getMonth() + 1).padStart(
        2,
        "0"
      )}/${date.getFullYear()}`,
    });
    const { orders, lastTransactions } = await degiroApi.getOrders({
      active: true,
      lastTransactions: true,
    });

    // console.log(orders);
    // console.log(lastTransactions);
    // console.log(data);

    const newOrders = await Promise.all(
      Object.keys(productsToBuy)
        .filter((productId) => productsToBuy[productId] > 0)
        // do not make orders if there open ones with the same ticker
        .filter((productId) => {
          const openOrdersForThisProductId = orders.filter((o: any) => o.productId == productId);
          const execute = openOrdersForThisProductId.length == 0;

          if (!execute) {
            console.log(
              `Order for product ID ${productId} will be skipped because there is at least an open order for the same product:`
            );
            console.log(openOrdersForThisProductId);
          }

          return execute;
        })
        // do not make orders if there are recent executed ones (same day and historical)
        .filter((productId) => {
          const transactionsForThisProductId = lastTransactions.filter(
            (t: any) => t.productId == productId
          );
          const historicalForThisProductId = data.filter(
            (o: any) => o.productId == productId && o.type == "CREATE" && o.status == "CONFIRMED"
          );
          const durations = [...transactionsForThisProductId, ...historicalForThisProductId]
            .map((t: any) => (t.date ? Date.parse(t.date) : Date.parse(t.last)))
            .map((date) => Date.now() - date) // difference in ms
            .map((difference) => difference / (1000 * 60 * 60 * 24)); // difference in days
          const execute = durations.every((duration) => duration >= lastOrder);

          if (!execute) {
            const first = durations.find((duration) => duration < lastOrder);
            console.log(
              `Order for product ID ${productId} will be skipped because there is a recent order (less than ${lastOrder.toFixed(
                2
              )} days): ${first?.toFixed(2)} days ago.`
            );
          }
          return execute;
        })
        .map(async (productId) => {
          const order: OrderType = {
            buySell: DeGiroActions.BUY,
            orderType: DeGiroMarketOrderTypes.MARKET,
            productId: productId,
            size: productsToBuy[productId],
            timeType: DeGiroTimeTypes.DAY,
          };

          try {
            const { confirmationId, transactionFees } = await degiroApi.createOrder(order);
            const orderId = await degiroApi.executeOrder(order, confirmationId);
            return { orderId: orderId, transactionFees: transactionFees };
          } catch (error) {
            console.error("Failed to create or execute order on the Degiro API.");
            res.json(error);
            res.statusCode = 500;
            return;
          }
        })
    );

    res.json(newOrders);
    res.statusCode = 200;
  } catch (error) {
    console.error("Failed to get orders from Degiro.");
    res.json({});
    res.statusCode = 500;
    return;
  }
}

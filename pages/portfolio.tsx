import Container from "@mui/material/Container";
import { GetServerSideProps } from "next";
import Head from "next/head";
import React from "react";
import { initialControlsState, controlsStateToAccountTypes } from "../components/common/controls";
import PortfolioSummary from "../components/portfolio/portfolio-summary";
import AccountsSummary from "../components/portfolio/totals";
import { AccountType, Currency } from "../library/types";
import { generatePortfolio, getAllDataWithRetries, PortfolioItem } from "./api/portfolio";
import { ThemeProvider } from "@mui/material/styles";

import theme from "../themes/dark";
import { Position } from "../library/api/positions";
import { sumBy } from "../library/arithmetic";
import AccountsApi from "../library/api/accounts";

type PortfolioServerSideProps = {
  positions: Position[];
  currency: Currency;
};

type PortfolioState = {
  accountTypes: AccountType[];
  portfolio: PortfolioItem[];
  totalValue: number;
};

// https://nextjs.org/docs/basic-features/data-fetching#getserversideprops-server-side-rendering
export const getServerSideProps: GetServerSideProps = async (_context) => {
  const currency: Currency = Currency[process.env.CURRENCY!];
  const positions = await getAllDataWithRetries(currency);

  return {
    props: {
      positions,
      currency,
    },
  };
};

export default class Portfolio extends React.Component<PortfolioServerSideProps, PortfolioState> {
  constructor(props: PortfolioServerSideProps) {
    super(props);
    this.onAccountsChange = this.onAccountsChange.bind(this);
    this.state = this.computeState(controlsStateToAccountTypes(initialControlsState));
  }

  computeState(accountTypes: AccountType[]) {
    const positions = AccountsApi.matchingPositionsByAccountType(
      this.props.positions,
      accountTypes
    );
    const portfolio = generatePortfolio(positions);
    const totalValue = sumBy(
      portfolio,
      (p) => p?.current,
      (_) => this.props.currency // this should be the same for all positions at this point
    );

    return { accountTypes, portfolio, totalValue };
  }

  onAccountsChange(accountTypes: AccountType[]) {
    this.setState(this.computeState(accountTypes), () => {
      console.info("State updated in root component");
    });
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <React.Fragment>
          <Head>
            <title>Portfolio</title>
          </Head>
          <Container maxWidth="lg">
            <AccountsSummary
              accountTypes={this.state.accountTypes}
              currency={this.props.currency}
              onAccountsChange={this.onAccountsChange}
              totalValue={this.state.totalValue}
            />
            <PortfolioSummary currency={this.props.currency} portfolio={this.state.portfolio} />
          </Container>
        </React.Fragment>
      </ThemeProvider>
    );
  }
}

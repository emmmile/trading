import React from "react";
import Head from "next/head";

export default function Empty({}) {
  return (
    <div>
      <Head>
        <title>Index</title>
        <link rel="icon" href="/favicon.png" />
        <meta name="ROBOTS" content="NOINDEX,NOFOLLOW"></meta>
      </Head>

      <main></main>
    </div>
  );
}

import { Currency, Percent, isCurrency } from "./types";

describe("isCurrency()", () => {
  const eur = Currency.EUR;
  const percent = Percent.PERCENT;

  it("works", () => {
    expect(isCurrency(eur)).toBe(true);
    expect(isCurrency(percent)).toBe(false);
  });

  it("toString() works", () => {
    expect(eur.toString()).toBe("EUR");
    expect(percent.toString()).toBe("%");
    expect(eur).toBe(Currency.EUR);
    expect(percent).toBe("%");
  });
});

import { createLogger, format, transports } from "winston";
import type { Format } from "logform";

const consoleFormat: Format = format.combine(format.colorize(), format.simple());

const INSTANCE = createLogger({
  transports: new transports.Console({
    level: process.env.LOG_LEVEL || "debug",
    format: consoleFormat,
  }),
});

export const log = INSTANCE;

import { Position } from "./api/positions";
import { Bank, Currency, InstrumentType } from "./types";
import AccountsApi from "./api/accounts";
import { hash } from "./arithmetic";

export class Esketit {
  static ESKETIT_API = "https://esketit.com/api/investor/account-summary";
  static ESKETIT_OAUTH = "https://esketit.com/api/investor/public/login";
  cookie: { authtoken: string; xcsrftoken: string };

  async getPosition(): Promise<Position> {
    const data = { currencyCode: "EUR" };
    const response = await fetch(Esketit.ESKETIT_API, {
      method: "POST",
      headers: {
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-origin",
        "sec-gpc": "1",
        cookie: this.cookie.authtoken + "; XSRF-TOKEN=" + this.cookie.xcsrftoken,
        "x-xsrf-token": this.cookie.xcsrftoken,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
    const json = await response.json();

    return {
      bank: Bank.ESKETIT,
      hashedAccountId: hash(AccountsApi.esketitAccountId()),
      acquiredValue: json.accountValue - json.totalIncome - json.totalPending,
      value: json.accountValue,
      instrumentType: InstrumentType.CASH,
      name: "Esketit",
      volume: json.totalInvestments,
      currency: Currency[Currency.EUR], // Esketit has currency in the response btw
    };
  }

  async login(credentials: { email: string; password: string }) {
    const response = await fetch(Esketit.ESKETIT_OAUTH, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(credentials),
    });

    let cookieHeaders = response.headers.get("set-cookie");
    const posXSRFID = cookieHeaders?.indexOf("XSRF-TOKEN=");
    let xcsrfHeader = cookieHeaders?.substr(posXSRFID!);
    const posSemicolonXCSRF = xcsrfHeader!.indexOf(";") - "XSRF-TOKEN=".length;
    xcsrfHeader = xcsrfHeader!.substr("XSRF-TOKEN=".length, posSemicolonXCSRF);
    const posJSESSIONID = cookieHeaders!.indexOf("auth_inv_token=");
    cookieHeaders = cookieHeaders!.substr(posJSESSIONID);
    const posSemicolon = cookieHeaders.indexOf(";");
    cookieHeaders = cookieHeaders.substr(0, posSemicolon);
    this.cookie = { authtoken: cookieHeaders, xcsrftoken: xcsrfHeader };
  }
}

export class IncomeMarketplace {
  static INCOME_URL = "https://backoffice.getincome.com/api/investor-details";
  static INCOME_OAUTH = "https://backoffice.getincome.com/api/auth0-login";
  accessToken: string;

  constructor() {}

  async login(credentials: { username: string; password: string }) {
    const response = await fetch(IncomeMarketplace.INCOME_OAUTH, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(credentials),
    });

    const json = await response.json();
    if (json.success != true) {
      throw new Error("Failed to authenticate with IncomeMarketplace");
    }

    this.accessToken = json.accessToken;
  }

  async getPosition(): Promise<Position> {
    const response = await fetch(IncomeMarketplace.INCOME_URL, {
      method: "GET",
      headers: { authorization: "Bearer " + this.accessToken },
    });

    const json = await response.json();

    return {
      bank: Bank.INCOME,
      hashedAccountId: hash(AccountsApi.incomeAccountId()),
      acquiredValue: json.funds.total - json.earnings.total_profit,
      value: json.funds.total,
      instrumentType: InstrumentType.CASH,
      name: "Income Marketplace",
      volume: null,
      currency: Currency[Currency.EUR],
    };
  }
}

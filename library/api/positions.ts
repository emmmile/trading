import DeGiro from "degiro-api";
import { PORTFOLIO_POSITIONS_TYPE_ENUM } from "degiro-api/dist/enums";
import ApiBase, { Avanza } from "./apibase";
import {
  avanzaInstrumentType,
  Bank,
  Currency,
  degiroInstrumentType,
  InstrumentType,
} from "../types";
import { hash, sumBy } from "../arithmetic";
import AccountsApi from "./accounts";
import { log } from "../logger";
import { PositionsResponse } from "../avanza";
import { Esketit, IncomeMarketplace } from "../p2p";

export type Position = {
  bank: Bank;
  hashedAccountId: string;
  acquiredValue?: number;
  value: number;
  instrumentType?: InstrumentType;
  name: string;
  volume: number | null;
  currency: Currency;
};

export default abstract class PositionsApi extends ApiBase {
  private static async avanzaPositions(avanza: Avanza): Promise<Position[]> {
    const positions: PositionsResponse = await avanza.call(
      "GET",
      `/_api/position-data/positions`,
      {}
    );
    // log.warn(JSON.stringify(positions, null, 2));

    const positionsData: Position[] = [];
    for (const position of positions.withOrderbook) {
      positionsData.push({
        bank: Bank.AVANZA,
        hashedAccountId: hash(position.account.id),
        acquiredValue: position.acquiredValue.value,
        value: position.value.value,
        instrumentType: avanzaInstrumentType(position.instrument.type),
        name: position.instrument.name,
        volume: position.volume.value,
        currency: Currency.SEK,
      });
    }

    for (const position of positions.cashPositions) {
      positionsData.push({
        bank: Bank.AVANZA,
        hashedAccountId: hash(position.account.id),
        value: position.totalBalance.value,
        instrumentType: InstrumentType.CASH,
        name: Currency.SEK.toString(),
        currency: Currency.SEK,
        volume: null,
      });
    }

    const total = sumBy(
      positionsData,
      (p) => p?.value,
      (p) => p?.currency
    );
    log.debug(
      `Found ${positionsData.length} Avanza positions (total ${total.toFixed(2)} ${Currency.SEK}).`
    );
    return positionsData;
  }

  // this is static because it's shared with the accounts API
  public static async degiroPositions(degiro: DeGiro): Promise<Position[]> {
    const dp = await degiro.getPortfolio({
      type: PORTFOLIO_POSITIONS_TYPE_ENUM.ALL,
      getProductDetails: true,
    });

    // All degiro positions are converted in EUR already
    // And also they are not updated I think, the value is the value at the previous close
    const positionsData: Position[] = dp.map((position) => {
      var thisCurrency: string = Currency.EUR;
      var name: string = Currency.EUR;
      var productType: InstrumentType = InstrumentType.CASH;
      var volume = null;
      const acquiredValue: number = -position.plBase[thisCurrency];
      const value: number = -position.todayPlBase[thisCurrency];

      if (position.productData) {
        // true for non-cash positions
        thisCurrency = Currency.EUR;
        productType = degiroInstrumentType(position.productData.productType);
        name = position.productData.name;
        volume = position.size;
      }

      return {
        bank: Bank.DEGIRO,
        hashedAccountId: hash(AccountsApi.degiroAccountId()),
        acquiredValue: acquiredValue,
        value: value,
        instrumentType: productType,
        name: name,
        volume: volume,
        currency: Currency[thisCurrency],
      };
    });

    const total = sumBy(
      positionsData,
      (p) => p?.value,
      (p) => p?.currency
    );
    log.debug(
      `Found ${positionsData.length} Degiro positions (total ${total.toFixed(2)} ${Currency.EUR}).`
    );
    return positionsData;
  }

  public static async otherPositions(
    incomeApi: IncomeMarketplace,
    esketit: Esketit
  ): Promise<Position[]> {
    const positions: Position[] = [];
    const bbvaAccountId = AccountsApi.bbvaAccountId();

    positions.push(await esketit.getPosition());
    positions.push(await incomeApi.getPosition());
    // Cash on BBVA (for now)
    positions.push({
      bank: Bank.BBVA,
      hashedAccountId: hash(bbvaAccountId),
      acquiredValue: 70000,
      value: 70340,
      instrumentType: InstrumentType.CASH,
      name: Currency.EUR,
      volume: null,
      currency: Currency[Currency.EUR],
    });
    // ESOP value at current company evaluation
    positions.push({
      bank: Bank.LEDGY,
      hashedAccountId: hash(AccountsApi.ledgyAccountId()),
      acquiredValue: 34800,
      value: 34800 * 2.1 * 0.75, // 2.1B USD, 0.75 probability of cashing out
      instrumentType: InstrumentType.SHARE,
      name: "Synthesia",
      volume: 796,
      currency: Currency[Currency.EUR],
    });
    // Approximate TFR in Synthesia, minus 20% taxes
    const yearsAtSynthesia =
      (new Date().getTime() - new Date("2023-09-05").getTime()) / (1000 * 60 * 60 * 24 * 365);
    const tfr = ((135000 * 1.02 ** (yearsAtSynthesia - 0.3333) * yearsAtSynthesia) / 13.5) * 0.8;
    positions.push({
      bank: Bank.BBVA, // For simplicity, no API for this (it will be cash)
      hashedAccountId: hash(bbvaAccountId),
      acquiredValue: tfr,
      value: tfr,
      instrumentType: InstrumentType.CASH,
      name: "TFR",
      volume: null,
      currency: Currency[Currency.EUR],
    });
    // Bitcoin on Coinbase, small position so it's not worth implementing an API
    positions.push({
      bank: Bank.COINBASE, // For simplicity, don't want to add API for this
      hashedAccountId: hash(AccountsApi.coinbaseAccountId()),
      acquiredValue: 0,
      value: 904, // TODO, it should be 0.00900367 BTC
      instrumentType: InstrumentType.CERTIFICATE,
      name: "BTC",
      volume: null,
      currency: Currency[Currency.EUR],
    });

    const total = sumBy(
      positions,
      (p) => p?.value,
      (p) => p?.currency
    );
    log.debug(
      `Found ${positions.length} other positions (total ${total.toFixed(2)} ${Currency.EUR}).`
    );
    return positions;
  }

  private async changeCurrency(positions: Position[], currency: Currency): Promise<Position[]> {
    for (const position of positions) {
      const rate = await this.conversionRate(position.currency, currency);

      position.currency = currency;
      position.acquiredValue = position.acquiredValue! * rate!;
      position.value = position.value * rate!;
    }

    return positions;
  }

  public async allPositions(currency: Currency): Promise<Position[]> {
    const avanza = await this.avanzaApi();
    const degiro = await this.degiroApi();
    const income = await this.incomeApi();
    const esketit = await this.esketitApi();
    const a = await PositionsApi.avanzaPositions(avanza);
    const d = await PositionsApi.degiroPositions(degiro);
    const o = await PositionsApi.otherPositions(income, esketit);
    return this.changeCurrency(a.concat(d).concat(o), currency);
  }

  static filterPositions(
    positions: Position[],
    accountsFilter?: string[],
    matchingNames?: string[]
  ): Position[] {
    var output: Position[] = [];

    // set would be better, but building a set for < 100 things
    // maybe it's even worse
    for (const position of positions) {
      if (accountsFilter && !accountsFilter.includes(position.hashedAccountId)) {
        continue;
      }
      if (matchingNames && !matchingNames.includes(position.name)) {
        continue;
      }

      output.push(position);
    }

    return output;
  }
}

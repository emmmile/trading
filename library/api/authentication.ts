import retry from "async-retry";
import Avanza from "avanza";
import DeGiro from "degiro-api";
import { log } from "../logger";
import { Esketit, IncomeMarketplace } from "../p2p";
import { TOTP } from "totp-generator";

export const avanzaCredentials = {
  username: process.env.AVANZA_USERNAME,
  password: process.env.AVANZA_PASSWORD,
  totpSecret: process.env.AVANZA_TOTPSECRET,
};

export const degiroCredentials = {
  username: process.env.DEGIRO_USERNAME,
  password: process.env.DEGIRO_PASSWORD,
  totpSecret: process.env.DEGIRO_TOTPSECRET,
};

export const incomeCredentials = {
  username: process.env.INCOME_USERNAME,
  password: process.env.INCOME_PASSWORD,
  totpSecret: process.env.INCOME_TOTPSECRET,
};

export const esketitCredentials = {
  username: process.env.ESKETIT_USERNAME,
  password: process.env.ESKETIT_PASSWORD,
  totpSecret: process.env.ESKETIT_TOTPSECRET,
};

const retryOptions = {
  retries: 3,
  factor: 2,
  minTimeout: 5000,
};

function emptyCheck(variableName: string, value: string | undefined | null) {
  if (!value || value.length == 0) {
    log.warn(`${variableName} is empty. Is the environment configured correctly?`);
  }
}

export async function avanzaAuthentication(retries: number = 1) {
  emptyCheck("AVANZA_USERNAME", avanzaCredentials.username);
  emptyCheck("AVANZA_PASSWORD", avanzaCredentials.password);
  emptyCheck("AVANZA_TOTPSECRET", avanzaCredentials.totpSecret);

  const api = new Avanza();

  retryOptions.retries = retries;
  const authResult = await retry(async (_) => {
    log.debug("Trying to authenticate with the Avanza API.");
    return api.authenticate(avanzaCredentials);
  }, retryOptions);

  // log.debug(authResult);
  log.info("Successfully authenticated with the Avanza API.");
  return api;
}

export async function degiroAuthentication(retries: number = 1): Promise<DeGiro> {
  emptyCheck("DEGIRO_USERNAME", degiroCredentials.username);
  emptyCheck("DEGIRO_PASSWORD", degiroCredentials.password);
  emptyCheck("DEGIRO_TOTPSECRET", degiroCredentials.totpSecret);

  async function checkLogin(api: DeGiro | undefined): Promise<boolean> {
    if (!api) {
      return false;
    }

    const loginResult: boolean | Promise<boolean> = api.isLogin();
    if (loginResult instanceof Promise) {
      return await loginResult;
    } else {
      return loginResult;
    }
  }

  // Attempt login only once to avoid risking account block.
  log.debug("Trying to authenticate with the Degiro API.");
  // https://github.com/bellstrand/totp-generator
  const { otp, expires } = TOTP.generate(degiroCredentials.totpSecret!);
  const api = new DeGiro({
    username: degiroCredentials.username,
    pwd: degiroCredentials.password,
    oneTimePassword: otp,
  });
  await api.login(); // discard the accounts data

  if (await checkLogin(api)) {
    log.info("Successfully authenticated with the Degiro API.");
    return api;
  } else {
    throw new Error(
      "Error authenticing with the Degiro API. Exiting process to avoid being locked out of the account."
    );
  }
}

export async function incomeAuthentication(retries: number = 3): Promise<IncomeMarketplace> {
  emptyCheck("INCOME_USERNAME", incomeCredentials.username);
  emptyCheck("INCOME_PASSWORD", incomeCredentials.password);
  // emptyCheck('DEGIRO_TOTPSECRET', degiroCredentials.totpSecret);

  const api = new IncomeMarketplace();

  retryOptions.retries = retries;
  await retry(async (_) => {
    log.debug("Trying to authenticate with the Income Marketplace API.");
    await api.login({
      username: incomeCredentials.username!,
      password: incomeCredentials.password!,
    });

    return api;
  }, retryOptions);

  log.info("Successfully authenticated with the Income Marketplace API.");
  return api;
}

export async function esketitAuthentication(retries: number = 3): Promise<Esketit> {
  emptyCheck("ESKETIT_USERNAME", esketitCredentials.username);
  emptyCheck("ESKETIT_PASSWORD", esketitCredentials.password);
  // emptyCheck('DEGIRO_TOTPSECRET', degiroCredentials.totpSecret);

  const api = new Esketit();

  retryOptions.retries = retries;
  await retry(async (_) => {
    log.debug("Trying to authenticate with the Esketit API.");
    await api.login({
      email: esketitCredentials.username!,
      password: esketitCredentials.password!,
    });

    return api;
  }, retryOptions);

  log.info("Successfully authenticated with the Esketit API.");
  return api;
}

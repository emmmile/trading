import { IncomeMarketplace, Esketit } from "../p2p";
import { Currency } from "../types";
import { Avanza } from "./apibase";
import CurrencyApi, { AccountInfoCurrencyPairs } from "./currency";
import DeGiro from "degiro-api";

const fakePairs: AccountInfoCurrencyPairs = {
  EURCHF: { id: 1366262, price: "1.5434" },
  MADEUR: { id: -1, price: "0.09500" },
  CHFDKK: { id: 11839952, price: "6.9823" },
  BGNEUR: { id: -1, price: "0.5113" },
  ROLEUR: { id: -1, price: "0.00002021" },
  EURSEK: { id: -1, price: "10.0" },
};

class TestCurrencyApi extends CurrencyApi {
  incomeApi(): Promise<IncomeMarketplace> {
    throw new Error("Method not implemented in test API.");
  }
  esketitApi(): Promise<Esketit> {
    throw new Error("Method not implemented in test API.");
  }
  avanzaApi(): Promise<Avanza> {
    throw new Error("Method not implemented in test API.");
  }
  degiroApi(): Promise<DeGiro> {
    throw new Error("Method not implemented in test API.");
  }
}

describe("conversionRate()", () => {
  // https://stackoverflow.com/questions/43265944/is-there-any-way-to-mock-private-functions-with-jest
  const test: CurrencyApi = new TestCurrencyApi();

  afterEach(jest.clearAllMocks);
  const spy = jest.spyOn(CurrencyApi.prototype as any, "currencyPairsFromDegiro");
  spy.mockResolvedValue(fakePairs);

  it("works in the identity case", async () => {
    expect(await test.conversionRate(Currency.EUR, Currency.EUR)).toBe(1.0);
    expect(spy).toHaveBeenCalledTimes(0);
  });

  it("uses caching correctly", async () => {
    try {
      await test.conversionRate(Currency.CAD, Currency.EUR);
      await test.conversionRate(Currency.USD, Currency.SEK);
      await test.conversionRate(Currency.EUR, Currency.CHF);
    } catch (e) {}

    expect(spy).toHaveBeenCalledTimes(1);
  });

  it("works when currency pair exist", async () => {
    const result = await test.conversionRate(Currency.EUR, Currency.CHF);
    expect(result).toBe(1.5434);
  });

  it(`throws when currency pair doesn't exist`, async () => {
    await expect(test.conversionRate(Currency.USD, Currency.EUR)).rejects.toThrow();
  });

  it(`works when inverse currency pair exist`, async () => {
    const result = await test.conversionRate(Currency.SEK, Currency.EUR);
    expect(result).toBeCloseTo(0.1, 6);
  });
});

import DeGiro from "degiro-api";
import { Currency } from "../types";
import { Esketit, IncomeMarketplace } from "../p2p";

// TODO type of the Avanza API, because it's not in typescript
export type Avanza = {
  call: (method: string, uri: string, data: any) => any;
};

export default abstract class ApiBase {
  abstract avanzaApi(): Promise<Avanza>;
  abstract degiroApi(): Promise<DeGiro>;
  abstract incomeApi(): Promise<IncomeMarketplace>;
  abstract esketitApi(): Promise<Esketit>;
  abstract conversionRate(a: Currency, b: Currency): Promise<number>;
}

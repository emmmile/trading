import { hash, sumBy } from "../arithmetic";
import { log } from "../logger";
import { AccountType, Bank, Currency } from "../types";
import ApiBase from "./apibase";
import { Position } from "./positions";

export type Account = {
  bank: Bank;
  hashedAccountId: string;
  accountType: AccountType;
  currentValue: number;
  totalPositionsValue: number;
  totalBalance: number;
  currency: Currency;
  performanceSinceOneYear?: number;
  performanceSinceThreeYears?: number;
};

export default abstract class AccountsApi extends ApiBase {
  static matchingPositionsByAccountType(positions: Position[], accountTypes: AccountType[]) {
    const accountsData = this.allHashedAccountIdsWithType();

    return positions.filter((p) => {
      const thisPositionAccountType = accountsData[p.hashedAccountId];
      return accountTypes.includes(thisPositionAccountType);
    });
  }

  static allAccountIds(): string[] {
    return this.avanzaAccountIds().concat([
      this.degiroAccountId(),
      this.esketitAccountId(),
      this.incomeAccountId(),
      this.bbvaAccountId(),
      this.ledgyAccountId(),
      this.coinbaseAccountId(),
    ]);
  }

  static allHashedAccountIdsWithType(): { [account: string]: AccountType } {
    const avanzaAccounts = require("../../data/avanza-account-ids.json");
    const accountsWithType = { ...avanzaAccounts };
    for (const account of this.allAccountIds()) {
      if (!accountsWithType[account]) {
        accountsWithType[account] = AccountType.SAVINGS;
      }
    }

    return Object.keys(accountsWithType).reduce((result, accountId) => {
      result[hash(accountId)] = accountsWithType[accountId];
      return result;
    }, {});
  }

  static avanzaAccountIds(): string[] {
    const accountsMap = require("../../data/avanza-account-ids.json");
    return Object.keys(accountsMap);
  }

  static degiroAccountId(): string {
    return "degiro";
  }

  static esketitAccountId(): string {
    return "esketit";
  }

  static incomeAccountId(): string {
    return "income";
  }

  static bbvaAccountId(): string {
    return "bbva";
  }

  static ledgyAccountId(): string {
    return "ledgy";
  }

  static coinbaseAccountId(): string {
    return "coinbase";
  }

  static hashedAccountIds(ids: string[]): string[] {
    return ids.map((id) => hash(id));
  }
}

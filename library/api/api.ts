import DeGiro from "degiro-api";
import { Mixin } from "ts-mixer";
import AccountsApi from "./accounts";
import {
  avanzaAuthentication,
  degiroAuthentication,
  esketitAuthentication,
  incomeAuthentication,
} from "./authentication";
import CurrencyApi from "./currency";
import PositionsApi from "./positions";
import { Esketit, IncomeMarketplace } from "../p2p";

export default class Api extends Mixin(PositionsApi, CurrencyApi, AccountsApi) {
  private static API: Api;

  private degiro;
  private avanza;
  private income;
  private esketit;

  private constructor() {
    super();
    this.degiro = degiroAuthentication(1);
    this.avanza = avanzaAuthentication(3);
    this.income = incomeAuthentication(3);
    this.esketit = esketitAuthentication(3);
  }

  async avanzaApi() {
    return await this.avanza;
  }

  async degiroApi(): Promise<DeGiro> {
    return await this.degiro;
  }

  async incomeApi(): Promise<IncomeMarketplace> {
    return await this.income;
  }

  async esketitApi(): Promise<Esketit> {
    return await this.esketit;
  }

  static create(): Api {
    if (!this.API) {
      this.API = new Api();
    }

    return this.API;
  }
}

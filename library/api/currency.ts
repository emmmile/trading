import { AccountInfoType } from "degiro-api/dist/types";
// https://stackoverflow.com/questions/51269431/jest-mock-inner-function
import { Currency } from "../types";
import ApiBase from "./apibase";
import { log } from "../logger";

// copied from the Degiro API, because the type it's not exported
export type AccountInfoCurrencyPairs = {
  [key: string]: {
    id: number;
    price: string;
  };
};

export default abstract class CurrencyApi extends ApiBase {
  private cachedCurrencyPairs: AccountInfoCurrencyPairs;

  async currencyPairsFromDegiro(): Promise<AccountInfoCurrencyPairs> {
    const degiro = await this.degiroApi();
    const info: AccountInfoType = await degiro.getAccountInfo();
    return info.currencyPairs;
  }

  private async conversionRateStr(t: string): Promise<number | undefined> {
    // Otherwise use Degiro to get the currency pairs
    if (!this.cachedCurrencyPairs) {
      this.cachedCurrencyPairs = await this.currencyPairsFromDegiro();
    }

    // Check if we have it in the pairs
    if (this.cachedCurrencyPairs[t]) {
      return +this.cachedCurrencyPairs[t].price;
    } else {
      return undefined;
    }
  }

  public async conversionRate(a: Currency, b: Currency): Promise<number> {
    if (a + b == b + a) {
      return 1.0;
    }

    const rate = await this.conversionRateStr(a + b);
    const inverse = await this.conversionRateStr(b + a);
    if (rate) {
      return rate;
    } else if (inverse) {
      return 1.0 / inverse;
    } else {
      throw new Error(`Conversion rate pair '${a}${b}' not found.`);
    }
  }
}

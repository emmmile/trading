import { OrderbookType } from "./avanza";

export enum Currency {
  EUR = "EUR",
  SEK = "SEK",
  USD = "USD",
  CHF = "CHF",
  CAD = "CAD",
}

export enum Percent {
  PERCENT = "%",
}

export function isCurrency(x: Currency | Percent): boolean {
  return x != Percent.PERCENT;
}

export enum InstrumentType {
  SHARE = "SHARE",
  ETF = "ETF",
  FUND = "FUND",
  CERTIFICATE = "CERTIFICATE",
  CASH = "CASH",
}

export function avanzaInstrumentType(t: string): InstrumentType {
  switch (t) {
    case OrderbookType.Stock:
      return InstrumentType.SHARE;
    case OrderbookType.ExchangeTradedFund:
      return InstrumentType.ETF;
    case OrderbookType.Fund:
      return InstrumentType.FUND;
    case OrderbookType.Certificate:
      return InstrumentType.CERTIFICATE;
    default:
      throw Error(`Failed to parse Avanza string '${t}' as InstrumentType.`);
  }
}

export function degiroInstrumentType(t: string): InstrumentType {
  switch (t) {
    case "STOCK":
      return InstrumentType.SHARE;
    case "ETF":
      return InstrumentType.ETF;
    case "FUND":
      return InstrumentType.FUND;
    // TODO there are probably other types that I'm not currently using
    default:
      throw Error(`Failed to parse Degiro string '${t}' as InstrumentType.`);
  }
}

export enum Bank {
  AVANZA = "AVANZA",
  BBVA = "BBVA",
  DEGIRO = "DEGIRO",
  ESKETIT = "ESKETIT",
  ETRADE = "ETRADE",
  INCOME = "INCOME",
  LEDGY = "LEDGY",
  COINBASE = "COINBASE",
}

export enum AccountType {
  PENSION = "PENSION",
  ISK = "SAVINGS/ISK",
  SAVINGS = "SAVINGS",
}

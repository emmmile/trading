import crypto from "crypto";
import { Currency } from "./types";

export function sum(a: number, b: number): number {
  return a + b;
}

// https://stackoverflow.com/questions/1960473/get-all-unique-values-in-a-javascript-array-remove-duplicates
export function sameCurrency<R>(
  v: (R | null | undefined)[],
  currencyGetter: (_: R | null | undefined) => Currency | null | undefined,
  filterFalsy: (_: any | null | undefined) => boolean = (_) => true
): boolean {
  const unique = (value: Currency, index: number, self: Currency[]) =>
    self.indexOf(value) === index;

  return v.map(currencyGetter).filter(filterFalsy).filter(unique).length <= 1;
}

export function sumBy<R>(
  v: (R | null | undefined)[],
  valueGetter: (_: R | null | undefined) => number | null | undefined,
  currencyGetter: undefined | ((_: R | null | undefined) => Currency | null | undefined),
  filterFalsy: (_: any | null | undefined) => boolean = (_) => true
): number {
  if (currencyGetter && !sameCurrency(v, currencyGetter, filterFalsy)) {
    throw new Error("Trying to sum array with different currencies.");
  }

  return v
    .map(valueGetter) // use fn to get the value from the object
    .filter(filterFalsy) // filter out falsy values, if necessary
    .reduce(sum, 0)!;
}

export function hash(id: string): string {
  return crypto.createHash("sha256").update(id).digest("hex");
}

// Positions endpoint
// https://www.avanza.se/_api/position-data/positions

export interface PositionsResponse {
  withOrderbook: WithOrderbook[];
  cashPositions: CashPosition[];
  withoutOrderbook: unknown[];
}

export interface CashPosition {
  account: AvanzaAccount;
  totalBalance: TotalBalance;
  id: string;
}

export interface AvanzaAccount {
  id: string;
  type: AvanzaAccountType;
  name: string;
  urlParameterId: string;
  hasCredit: boolean;
}

export enum AvanzaAccountType {
  Aktiefondkonto = "AKTIEFONDKONTO",
  Investeringssparkonto = "INVESTERINGSSPARKONTO",
  Tjanstepension = "TJANSTEPENSION",
}

export interface TotalBalance {
  value: number;
  unit: Unit;
  unitType: UnitType;
  decimalPrecision: number;
}

export enum Unit {
  EMPTY = "",
  EUR = "EUR",
  PERCENTAGE = "percentage",
  SEK = "SEK",
  USD = "USD",
}

export enum UnitType {
  Monetary = "MONETARY",
  Percentage = "PERCENTAGE",
  Unitless = "UNITLESS",
}

export interface WithOrderbook {
  account: AvanzaAccount;
  instrument: Instrument;
  volume: TotalBalance;
  value: TotalBalance;
  averageAcquiredPrice: TotalBalance;
  averageAcquiredPriceInstrumentCurrency: TotalBalance;
  acquiredValue: TotalBalance;
  lastTradingDayPerformance: LastTradingDayPerformance;
  id: string;
  // allow extra properties
  [x: string]: unknown;
}

export interface Instrument {
  id: string;
  type: OrderbookType;
  name: string;
  orderbook: Orderbook;
  currency: Unit;
  isin: string;
  volumeFactor: number;
}

export interface Orderbook {
  id: string;
  flagCode: FlagCode | null;
  name: string;
  type: OrderbookType;
  tradeStatus: TradeStatus;
  quote: Quote;
  turnover: Turnover;
  lastDeal: LastDeal;
}

export enum FlagCode {
  De = "DE",
  SE = "SE",
  Us = "US",
}

export interface LastDeal {
  date: Date;
  time: null;
}

export interface Quote {
  highest: TotalBalance;
  lowest: TotalBalance;
  buy: TotalBalance | null;
  sell: TotalBalance | null;
  latest: TotalBalance;
  change: TotalBalance;
  changePercent: TotalBalance;
  updated: Date;
}

export enum TradeStatus {
  BuyableAndSellable = "BUYABLE_AND_SELLABLE",
}

export interface Turnover {
  volume: TotalBalance;
  value: TotalBalance | null;
}

export enum OrderbookType {
  Certificate = "CERTIFICATE",
  ExchangeTradedFund = "EXCHANGE_TRADED_FUND",
  Fund = "FUND",
  Stock = "STOCK",
}

export interface LastTradingDayPerformance {
  absolute: TotalBalance;
  relative: TotalBalance;
}

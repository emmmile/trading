import { sumBy } from "./arithmetic";
import { Currency } from "./types";

describe("sumBy()", () => {
  const falsyList = [null, undefined, NaN];
  const simpleData = [{ k: NaN }, { k: null }, { k: 42 }, { k: undefined }];
  const dataWithSameCurrency = [
    { k: NaN, currency: Currency.EUR },
    { k: null, currency: Currency.EUR },
    { k: 1, currency: Currency.EUR },
    { k: undefined, currency: Currency.EUR },
    { k: 2, currency: Currency.EUR },
  ];
  const dataWithDifferentCurrency = [
    { k: 1, currency: Currency.EUR },
    { k: undefined, currency: Currency.EUR },
    { k: 2, currency: Currency.SEK },
  ];

  it("works with empty input", () => {
    expect(sumBy([], (x) => x, undefined, Boolean)).toBe(0);
  });

  it("works with falsy values", () => {
    // with filtering of falsy values (empty list)
    expect(sumBy(falsyList, (x) => x, undefined, Boolean)).toBe(0);
    expect(sumBy(simpleData, (x) => x?.k, undefined, Boolean)).toBe(42);
    // no filtering
    expect(
      sumBy(
        falsyList,
        (x) => x,
        undefined,
        (x) => true
      )
    ).toBe(NaN);
  });

  it("actually adds up numbers", () => {
    expect(sumBy([1, 2, 3], (x) => x, undefined, Boolean)).toBe(6);
  });

  it("works with the same currency", () => {
    expect(
      sumBy(
        [],
        (x) => x,
        (x) => Currency.EUR,
        Boolean
      )
    ).toBe(0);
    expect(
      sumBy(
        dataWithSameCurrency,
        (x) => x?.k,
        (x) => Currency.EUR,
        Boolean
      )
    ).toBe(3);
  });

  it("discard currencies with empty filter", () => {
    expect(sumBy(dataWithDifferentCurrency, (x) => x?.k, undefined, Boolean)).toBe(3);
  });

  it("throws with different currencies", () => {
    expect(() =>
      sumBy(
        dataWithDifferentCurrency,
        (x) => x?.k,
        (x) => x?.currency,
        Boolean
      )
    ).toThrow(Error);
  });
});

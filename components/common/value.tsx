import React from "react";
import { Grid, Typography, TypographyVariant } from "@mui/material";
import { Currency, Percent, isCurrency } from "../../library/types";

type ValueProps = {
  value: number;
  unit: Currency | Percent;
  variant: TypographyVariant;
  // TODO. This is copied from MUI. Is there a type for this?
  color?:
    | "initial"
    | "inherit"
    | "primary"
    | "secondary"
    | "textPrimary"
    | "textSecondary"
    | "error";
  className?: any; // TODO
  blur?: number;
};

export default class Value extends React.Component<ValueProps, any> {
  constructor(props: ValueProps) {
    super(props);
  }

  render() {
    const value = this.props.value;
    const unit = this.props.unit;
    const variant = this.props.variant;
    const precision = isCurrency(unit) ? 0 : 2;
    const color = this.props.color;
    const className = this.props.className;
    const blur = this.props.blur ?? 0;

    return (
      <Grid item className={className}>
        <Typography
          variant={variant}
          display="inline"
          color={color}
          style={{
            filter: "blur(" + blur + "px)",
          }}>
          {value.toFixed(precision)}
        </Typography>
        <Typography
          variant={variant}
          display="inline"
          color={color}
          // TODO. The fuck, remove this and move it to theme.
          style={{
            textTransform: "uppercase",
            paddingLeft: "0.1em",
            opacity: 0.3,
            filter: "blur(" + blur * 0.8 + "px)",
          }}>
          {unit.toString()}
        </Typography>
      </Grid>
    );
  }
}

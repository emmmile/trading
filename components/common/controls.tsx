import React from "react";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { AccountType } from "../../library/types";

type ControlsProps = {
  onAccountsChange: (accountTypes: AccountType[]) => void;
};

type ControlsState = {
  savings: boolean;
  pension: boolean;
};

export const initialControlsState: ControlsState = {
  savings: true,
  pension: true,
};

export function controlsStateToAccountTypes(controlsState: ControlsState): AccountType[] {
  const accountTypes: AccountType[] = [];

  if (controlsState.savings) {
    accountTypes.push(AccountType.ISK);
    accountTypes.push(AccountType.SAVINGS);
  }

  if (controlsState.pension) {
    accountTypes.push(AccountType.PENSION);
  }

  return accountTypes;
}

export default class Controls extends React.Component<ControlsProps, ControlsState> {
  constructor(props: ControlsProps) {
    super(props);
    this.state = initialControlsState;
    this.handleControlsChange = this.handleControlsChange.bind(this);
  }

  handleControlsChange(event) {
    const target = event.target;
    const value: boolean = target.type === "checkbox" ? target.checked : target.value;
    const name: "savings" | "pension" = target.name;

    let state: ControlsState;
    if (name == "savings") {
      state = {
        savings: value,
        pension: this.state.pension,
      };
    } else {
      state = {
        savings: this.state.savings,
        pension: value,
      };
    }

    this.setState(state, () => {
      this.props.onAccountsChange(controlsStateToAccountTypes(state));
    });
  }

  render() {
    return (
      <FormGroup row>
        <FormControlLabel
          control={
            <Checkbox
              checked={this.state.savings}
              onChange={this.handleControlsChange}
              name="savings"
              color="primary"
            />
          }
          label="savings"
        />
        <FormControlLabel
          control={
            <Checkbox
              checked={this.state.pension}
              onChange={this.handleControlsChange}
              name="pension"
              color="error"
            />
          }
          label="pension"
        />
      </FormGroup>
    );
  }
}

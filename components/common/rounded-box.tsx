import React from "react";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import withStyles from "@mui/styles/withStyles";
import { styles } from "../../themes/dark";

type RoundedBoxProps = {
  rows: any;
  alignItems: any;
  classes: any;
};

class RoundedBox extends React.Component<RoundedBoxProps> {
  constructor(props: RoundedBoxProps) {
    super(props);
  }

  render() {
    const rows = this.props.rows;
    const alignItems = this.props.alignItems;
    const classes = this.props.classes;

    return (
      <Box p={3} mt={2} className={classes.roundedBox}>
        <Grid item container spacing={1} alignItems={alignItems}>
          {rows}
        </Grid>
      </Box>
    );
  }
}

export default withStyles(styles)(RoundedBox);

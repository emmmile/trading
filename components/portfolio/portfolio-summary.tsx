import React from "react";
import { Currency } from "../../library/types";
import { PortfolioItem } from "../../pages/api/portfolio";
import PortfolioRow from "./portfolio-row";
import { sumBy } from "../../library/arithmetic";

export type PortfolioSummaryProps = {
  currency: Currency;
  portfolio: PortfolioItem[];
};

export default class PortfolioSummary extends React.Component<PortfolioSummaryProps> {
  constructor(props: PortfolioSummaryProps) {
    super(props);
  }

  render() {
    const totalValue = sumBy(
      this.props.portfolio,
      (p) => p?.current,
      (_) => this.props.currency // this should be the same for all positions at this point
    );

    return this.props.portfolio.flatMap((pi, index) => {
      return (
        <PortfolioRow
          totalValue={totalValue}
          portfolioItem={pi}
          portfolioItemName={pi.name}
          key={1 + index}
          currency={this.props.currency}
        />
      );
    });
  }
}

import React from "react";
import Grid from "@mui/material/Grid";
import Value from "../common/value";
import Typography from "@mui/material/Typography";
import withStyles from "@mui/styles/withStyles";
import { styles } from "../../themes/dark";
import { Currency } from "../../library/types";

type PositionProps = {
  positionName: string;
  positionValue: number;
  classes: any;
  currency: Currency;
};

class Position extends React.Component<PositionProps> {
  constructor(props: PositionProps) {
    super(props);
  }

  render() {
    const positionName = this.props.positionName;
    const positionValue = this.props.positionValue;
    const classes = this.props.classes;

    return [
      <Grid item xs={12} sm={6} key={1} className={classes.positionName}>
        <Typography variant="body1">{positionName}</Typography>
      </Grid>,
      <Grid item xs={12} sm={6} key={2} className={classes.positionValue}>
        <Value variant="body1" value={positionValue} unit={this.props.currency} blur={0} />
      </Grid>,
    ];
  }
}

export default withStyles(styles)(Position);

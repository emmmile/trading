import React from "react";
import Controls from "../common/controls";
import RoundedBox from "../common/rounded-box";
import Value from "../common/value";
import { Grid, Typography } from "@mui/material";
import withStyles from "@mui/styles/withStyles";
import { styles } from "../../themes/dark";
import { AccountType, Currency } from "../../library/types";

type AccountsSummaryProps = {
  accountTypes: AccountType[];
  classes: any;
  currency: Currency;
  onAccountsChange: (accountTypes: AccountType[]) => void;
  totalValue: number;
};

class AccountsSummary extends React.Component<AccountsSummaryProps> {
  constructor(props: AccountsSummaryProps) {
    super(props);
  }

  render() {
    const classes = this.props.classes;
    const totalsAndControlsRow = [
      <Grid item xs={12} sm={6} key={0}>
        <Typography variant="caption" className={classes.description}>
          selected accounts
        </Typography>
      </Grid>,
      <Grid item xs={12} sm={6} key={1}>
        <Controls onAccountsChange={this.props.onAccountsChange} />
      </Grid>,
      <Grid item xs={12} sm={6} key={2}>
        <Typography variant="h3">total</Typography>
      </Grid>,
      <Grid item xs={12} sm={6} key={3}>
        <Value variant="h3" value={this.props.totalValue} unit={this.props.currency} blur={0} />
      </Grid>,
    ];

    return <RoundedBox alignItems="flex-end" rows={[totalsAndControlsRow]} key={0} />;
  }
}

export default withStyles(styles)(AccountsSummary);

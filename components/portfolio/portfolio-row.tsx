import React from "react";
import Grid from "@mui/material/Grid";
import Value from "../common/value";
import { Typography } from "@mui/material";
import RoundedBox from "../common/rounded-box";
import Position from "./position";
import withStyles from "@mui/styles/withStyles";
import { styles } from "../../themes/dark";
import { PortfolioItem } from "../../pages/api/portfolio";
import { Currency, Percent } from "../../library/types";

export type PortfolioRowProps = {
  portfolioItemName: string;
  portfolioItem: PortfolioItem;
  totalValue: number;
  classes: any;
  key: number;
  currency: Currency;
};

class PortfolioRow extends React.Component<PortfolioRowProps> {
  constructor(props: PortfolioRowProps) {
    super(props);
  }

  render() {
    const classes = this.props.classes;
    const targetValue = (this.props.totalValue * this.props.portfolioItem.targetPercent) / 100.0;
    const isFarFromTarget =
      Math.abs(this.props.portfolioItem.targetPercent - this.props.portfolioItem.currentPercent) >
      3;

    const summaryRow = [
      <Grid item xs={12} sm={6} key={0}>
        <Typography variant="caption" className={classes.description}>
          portfolio category
        </Typography>
        <Typography variant="h4">{this.props.portfolioItemName}</Typography>
      </Grid>,
      <Grid item xs={12} sm={3} key={1}>
        <Typography variant="caption" className={classes.description}>
          current
        </Typography>
        <Value
          variant="h4"
          color={isFarFromTarget ? "error" : undefined}
          value={this.props.portfolioItem.currentPercent}
          unit={Percent.PERCENT}
        />
        <Value
          variant="h5"
          color={isFarFromTarget ? "error" : undefined}
          value={this.props.portfolioItem.current}
          unit={this.props.currency}
          blur={0}
        />
      </Grid>,
      <Grid item xs={12} sm={3} key={2}>
        <Typography variant="caption" className={classes.description}>
          target
        </Typography>
        <Value
          variant="h4"
          value={this.props.portfolioItem.targetPercent}
          unit={Percent.PERCENT}
          className={classes.targetColumn}
        />
        <Value
          variant="h5"
          value={targetValue}
          unit={this.props.currency}
          className={classes.targetColumn}
          blur={0}
        />
      </Grid>,
    ];

    const nameValuePairs = positionsToSortedRows(this.props.portfolioItem);
    const positionRows = nameValuePairs.flatMap((nv, index) => {
      const [name, value] = nv;
      return (
        <Position
          positionName={name}
          positionValue={value}
          key={3 + 2 * index}
          currency={this.props.currency}
        />
      );
    });

    return (
      <RoundedBox
        alignItems="flex-start"
        rows={[summaryRow, ...positionRows]}
        key={this.props.key + 1}
      />
    );
  }
}

export default withStyles(styles)(PortfolioRow);

function positionsToSortedRows(portfolioItem: PortfolioItem): Array<[string, number]> {
  const allPositions = portfolioItem.byAccount.flatMap((a) => a.matchingPositions);

  const positionsValueByName: { [x: string]: number } = {};
  for (const position of allPositions) {
    if (position.name in positionsValueByName) {
      positionsValueByName[position.name] += position.value;
    } else {
      positionsValueByName[position.name] = position.value;
    }
  }

  return [...Object.entries(positionsValueByName)].sort((a, b) => b[1] - a[1]);
}

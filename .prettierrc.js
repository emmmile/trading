module.exports = {
  bracketSameLine: true,
  printWidth: 100,
  singleQuote: false,
  tabWidth: 2,
  trailingComma: "es5",
};

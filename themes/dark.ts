import { createTheme, Theme, adaptV4Theme } from "@mui/material/styles";

const descriptionWeight = 800;
const descriptionOpacity = 0.4;
const descriptionFontSize = "0.75rem";

const positionsOpacity = 0.7;
const positionFontSize = "1rem";

const theme: Theme = createTheme({
  palette: {
    mode: "dark",
    primary: {
      main: "#42a5f5",
    },
    error: {
      main: "#ef5350",
    },
    text: {
      primary: "#fffaed", // cornslik, but a bit whiter
    },
    background: {
      default: "#252525",
    },
  },
  typography: {
    fontWeightRegular: 600,
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      "Segoe UI",
      "Roboto",
      "Oxygen",
      "Ubuntu",
      "Cantarell",
      "Fira Sans",
      "Droid Sans",
      "Helvetica Neue",
      "sans-serif",
    ].join(","),
    allVariants: {
      color: "#fffaed",
      textTransform: "uppercase",
    },
    // used for the position details
    body1: {
      fontWeight: 400,
      fontSize: "1.2em",
      textTransform: "none",
    },
    // captions are the descriptions on the fields
    caption: {
      fontWeight: descriptionWeight,
      fontSize: descriptionFontSize,
    },
  },
  // TODO: how to set the old overrides?
  components: {
    MuiFormControlLabel: {
      styleOverrides: {
        root: {
          // this brings the control labels at the same level as the top description
          marginBottom: "-0.8em",
        },
        label: {
          textTransform: "uppercase",
          opacity: descriptionOpacity,
          fontWeight: descriptionWeight,
          fontSize: descriptionFontSize, // same as caption
        },
      },
    },
  },
});

export default theme;

export const styles = {
  roundedBox: {
    backgroundColor: "#353535",
    borderRadius: "1em",
    border: "4px solid #353535",
    "&:hover": {
      border: "4px solid #545454",
    },
    // TODO. How the hell do you do this with MUI?
    // https://material-ui.com/styles/advanced/#jss-plugins
    "&:hover > div > div > span": {
      opacity: descriptionOpacity,
    },
    // Fuck React an MUI. This is nuts. Just want to fucking use a class name I defined.
    // https://stackoverflow.com/questions/25005703/how-to-use-nth-child-in-css-to-select-all-elements-after-the-3rd-one
    "&:hover:nth-child(n+2) > div > div:nth-child(n+4)": {
      display: "initial",
    },
  },
  positionName: {
    display: "none",
    opacity: positionsOpacity,
    fontSize: positionFontSize,
  },
  positionValue: {
    display: "none",
    opacity: positionsOpacity,
  },
  description: {
    opacity: 0,
  },
  targetColumn: {
    opacity: descriptionOpacity,
  },
};
